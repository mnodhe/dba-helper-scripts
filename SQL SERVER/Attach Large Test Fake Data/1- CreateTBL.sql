--create a database named SQL2 with 15G for data and 1G for Log and data autogrowth 200m and autogrowth 50m log
-- and create people table 
USE [SQL2]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO CREATE TABLE [dbo].[People](
		[PeopleId] [int] NOT NULL,
		[FName] [varchar](20) NULL,
		[LName] [varchar](20) NULL,
		[Address1] [varchar](50) NULL,
		[City] [varchar](20) NULL,
		[states] [varchar](20) NULL,
		[Zip] [varchar](20) NULL,
		[Country] [varchar](20) NULL,
		[Phone] [varchar](20) NULL
	) ON [PRIMARY]
GO