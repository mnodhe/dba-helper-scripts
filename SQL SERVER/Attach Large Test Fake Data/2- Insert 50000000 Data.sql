-- it May Take up to 20 minutes
--if your ram is below 8 you want to change 50000000 to 1000000 and run it 50 times
-- Also you can use redgate suit if you have below SQL server 2014
-- Be Patient :)
use SQL2
go
declare @id int
select @id = 1 while @id >= 1
    and @id <= 50000000 begin
insert into [dbo].[People]
values(
        (
            select @id
        ),
        'ali' + LTRIM(
            STR(
                (
                    select @id
                )
            )
        ),
        'naghie' + LTRIM(
            STR(
                (
                    select @id
                )
            )
        ),
        'iran tehran akbarabad' + LTRIM(
            STR(
                (
                    select @id
                )
            )
        ),
        'akbarabad' + LTRIM(
            STR(
                (
                    select @id
                )
            )
        ),
        'tehran' + LTRIM(
            STR(
                (
                    select @id
                )
            )
        ),
        'iran',
        'IR',
        '0912074' + LTRIM(
            STR(
                (
                    select @id
                )
            )
        )
    )
select @id = @id + 1
end