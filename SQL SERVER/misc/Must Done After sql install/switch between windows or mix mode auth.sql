-- enable windows authentication (require a restart service)
use master
go exec xp_instance_regwriter N 'HKEY_LOCAL_MACHINE',
    N'Software\Microsoft\MSSQLServer\MSSQLServer',
    N'LoginMode',
    REG_DWORD,
    1
go 
-- enable mixed mode authentication (require a restart service)
go exec xp_instance_regwriter N 'HKEY_LOCAL_MACHINE',
    N'Software\Microsoft\MSSQLServer\MSSQLServer',
    N'LoginMode',
    REG_DWORD,
    2
go 