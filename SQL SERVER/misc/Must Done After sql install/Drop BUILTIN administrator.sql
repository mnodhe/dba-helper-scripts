-- info about BUILTIN\Administrators
exec master..xp_logininfo @acctname = 'BUILTIN\Administrators',
@option = 'members';

--drop the BUILTIN\Administrators
 use master if exists (
    select *
    from sys.server_principals
    where name = N 'BUILTIN\Administrators'
) drop login [BUILTIN\Administrators]
go;

-- verify the BUILTIN\Administrators group hase been drop
exec master..xp_logininfo @acctname = 'BUILTIN\Administrators',
@option = 'members';

--NOTE : if you want to add BUILTIN\Administrators !!!!
exec sp_grantlogin 'BUILTIN\Administrators'
exec sp_addsrvrolemember 'BUILTIN\Administrators','sysadmin';


 