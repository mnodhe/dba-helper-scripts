----------------------------- NOOOTEE DANGERR !!!!! -----------------------------
--MAKE SURE YOU HAVE A LOGIN THAT IS PART OF THE SYSADMIN ROLE BEFORE DOING THIS
----------------------------- NOOOTEE DANGERR !!!!! -----------------------------
use master
go alter login [sa] with PASSWORD = N 'NewStrongpass'
go alter login [sa] DISABLE
go 
-- IF YOU WANT TO ENABLE IT AGAIN
use master
go alter login [sa] with PASSWORD = N 'NewStrongpass'
go alter login [sa] ENABLE
go