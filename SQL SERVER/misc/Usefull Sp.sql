------log and db file Path and size and maxsize info-------
sp_helpdb [DatabaseName]
----------------------------------------------------------
--------------show all db's log space usage---------------
DBCC sqlPerf (logspace)
----------------------------------------------------------
------VLF(Virtual Log File) info each row is one vlf------
DBCC LOGINFO
----------------------------------------------------------
-------------- show list of  process ---------------------
sp_who 
----------------- kill a process id ----------------------
kill 1
----------------------------------------------------------
------------- see all index in a table -------------------
sp_helpindex '[database].[tablename]'
----------------------------------------------------------
---------------- see fragmentation percent ---------------
select [avg_fragmentation_in_percent] 
from sys_dm_db_index_physical_state
(
    DB_ID (N'ShrinkDB'),
    OBJECT_ID(N'second'),
    1,
    null,
    'limited'
);
----------------------------------------------------------