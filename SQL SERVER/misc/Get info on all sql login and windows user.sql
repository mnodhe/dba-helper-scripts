select name as Login_name,
    type_desc as account_type,
    is_disabled
from sys.server_principals
where TYPE in ('U', 'S', 'G')
order by name,
    type_desc