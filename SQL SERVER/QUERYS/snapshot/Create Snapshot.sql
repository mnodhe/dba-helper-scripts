sp_helpdb 'databasename';--need for name
create database Snapsho_DBS on --name of snapshot db
(
    name = 'databasename',
    -- name of the logical data file
    FileName = 'C:\Program Files\Microsoft SQL Server\MSSQL13.MSSQLSERVER\MSSQL\DATA\databasename_snapshot.ss'
) -- file location of snapshot with SS Extention.
as snapshot of SQLSIZEDB;
go;