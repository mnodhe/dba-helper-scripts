-- You Should Have Alerts for severity 17 to 25 
-- You should first Set a Database Mail  (See SetUp DB Mail)
-- 1 create Alert for all severitys
use msdb
go exec msdb.dbo.sp_add_alert @name = N'Error 17 Alert',
    @message_id = 0,
    @severity = 17,
    @enabled = 1,
    @delay_between_responses = 0,
    @include_event_description_in = 1;
go exec msdb.dbo.sp_add_alert @name = N'Error 18 Alert',
    @message_id = 0,
    @severity = 18,
    @enabled = 1,
    @delay_between_responses = 0,
    @include_event_description_in = 1;
go exec msdb.dbo.sp_add_alert @name = N'Error 19 Alert',
    @message_id = 0,
    @severity = 19,
    @enabled = 1,
    @delay_between_responses = 0,
    @include_event_description_in = 1;
go exec msdb.dbo.sp_add_alert @name = N'Error 20 Alert',
    @message_id = 0,
    @severity = 20,
    @enabled = 1,
    @delay_between_responses = 0,
    @include_event_description_in = 1;
go exec msdb.dbo.sp_add_alert @name = N'Error 21 Alert',
    @message_id = 0,
    @severity = 21,
    @enabled = 1,
    @delay_between_responses = 0,
    @include_event_description_in = 1;
go exec msdb.dbo.sp_add_alert @name = N'Error 22 Alert',
    @message_id = 0,
    @severity = 22,
    @enabled = 1,
    @delay_between_responses = 0,
    @include_event_description_in = 1;
go exec msdb.dbo.sp_add_alert @name = N'Error 23 Alert',
    @message_id = 0,
    @severity = 23,
    @enabled = 1,
    @delay_between_responses = 0,
    @include_event_description_in = 1;
go exec msdb.dbo.sp_add_alert @name = N'Error 24 Alert',
    @message_id = 0,
    @severity = 24,
    @enabled = 1,
    @delay_between_responses = 0,
    @include_event_description_in = 1;
go exec msdb.dbo.sp_add_alert @name = N'Error 25 Alert',
    @message_id = 0,
    @severity = 25,
    @enabled = 1,
    @delay_between_responses = 0,
    @include_event_description_in = 1;
go -- 2 create operator
    exec msdb.dbo.sp_add_operator @name = 'DBA_ADMIN',
    @enabled = 1,
    @email_address = 'mnodhe@gmail.com';
--replace with your email for the operator
go 
-- 3 map all alerts to operator
    exec msdb.dbo.sp_add_notification @alert_name = N'Error 17 Alert',
    @operator_name = 'DBA_ADMIN',
    @notification_method = 1;
go exec msdb.dbo.sp_add_notification @alert_name = N'Error 18 Alert',
    @operator_name = 'DBA_ADMIN',
    @notification_method = 1;
go exec msdb.dbo.sp_add_notification @alert_name = N'Error 19 Alert',
    @operator_name = 'DBA_ADMIN',
    @notification_method = 1;
go exec msdb.dbo.sp_add_notification @alert_name = N'Error 20 Alert',
    @operator_name = 'DBA_ADMIN',
    @notification_method = 1;
go exec msdb.dbo.sp_add_notification @alert_name = N'Error 21 Alert',
    @operator_name = 'DBA_ADMIN',
    @notification_method = 1;
go exec msdb.dbo.sp_add_notification @alert_name = N'Error 22 Alert',
    @operator_name = 'DBA_ADMIN',
    @notification_method = 1;
go exec msdb.dbo.sp_add_notification @alert_name = N'Error 23 Alert',
    @operator_name = 'DBA_ADMIN',
    @notification_method = 1;
go exec msdb.dbo.sp_add_notification @alert_name = N'Error 24 Alert',
    @operator_name = 'DBA_ADMIN',
    @notification_method = 1;
go exec msdb.dbo.sp_add_notification @alert_name = N'Error 25 Alert',
    @operator_name = 'DBA_ADMIN',
    @notification_method = 1;
go