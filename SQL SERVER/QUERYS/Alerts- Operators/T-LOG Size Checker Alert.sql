USE msdb
GO

------------- Alert [TLog Get Bigger] ------------------
EXEC msdb.dbo.sp_add_alert @name=N'TLog Get Bigger', 
		@message_id=0, 
		@severity=0, 
		@enabled=1, 
		@delay_between_responses=0, 
		@include_event_description_in=1, 
		@category_name=N'[Uncategorized]', 
		@performance_condition=N'Databases|Log File(s) Used Size (KB)|test|>|500', 
		@job_id=N'00000000-0000-0000-0000-000000000000'
GO


