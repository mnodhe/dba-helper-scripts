-- 1 create a sql role in database 
use [dbname]
go
create role [hr_tables]
go
-- 2 select securables(for table ) and add to sql role
use [dbname]
go
grant select on [tblname]
to [hr_tables]
go

-- 3 add sql login to database roles
use [dbname]

go
alter role [hr_tables]
add member [username]
go