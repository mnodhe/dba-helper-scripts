/*
 create a server role 
 grant create any database permission to server role 
 grant view any database permission to server role 
 add sql login
 
 */
use master
go create server role [junior dba manage db]
go use master
go
grant create any database -- can create database 
    to [junior dba manage db]
go use master
go
grant view any database --can view any database but not see any table !! no permisssion
    to [junior dba manage db]
go 
--add sql login to the bew server role
    alter server role [junior dba manage db]
add member [username]
go