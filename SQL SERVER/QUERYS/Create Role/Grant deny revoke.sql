--demo of grant , deny , revoke permission on table 

-- grant select on table 
use [dbname]
go
grant select on [table1]
to [username]
-- grant select on table #2
use [dbname]
go
grant select on [table2]
to [username],[username2] --can giv permission to multipple user

--revoke username1 from table1
use [dbname]
go
revoke select on [table1] 
to [username1] as [dbo]

-- add username1 to role db_datareader , so he can view all tables
use [dbname]
go
alter role [db_datareader] add member [username1]
go

-- now deny select on table1
-- he cannot see table1 even he has db_datareader permission
-- any deny permission SUPERESEDES other collective permissions
use [dbname]
go
deny select on [table1] to [username1]
go
-- revoke the deny on table1 (he can view product table again)
use [dbname]
go
revoke select on [table1] to [username1]
go