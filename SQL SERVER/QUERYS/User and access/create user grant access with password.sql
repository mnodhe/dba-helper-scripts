-- create a sql login not using windows user
use master
go
create login [username]
with password=N'pass123'--need to be complex
must_change,default_database=[master],
check_expiration=on,
check_policy=on
go 

use [databasename]
go 

create user [username] for login [username] --create sql login

use [databasename]
go 
alter role [db_datareader]--add sql login to database role db_datareader
add MEMBER [username]
go
