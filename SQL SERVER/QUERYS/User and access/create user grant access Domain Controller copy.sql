--creating a windows user for sql server
use master
go 
create login [user] -- create a sql login of windows user account to access the sql server only
from windows with default_database = [master],
    DEFAULT_LANGUAGE = [us_english]
go 

use dbname
go 
create user [username] --create a sql user using sql login to access the database
    for login [user] --sql login
    with default_schema = [dbo]
go

 use dbname
go
GRANT select --grant permission to sql user to access a table 
    on [table] to [username] as [dbo]
go