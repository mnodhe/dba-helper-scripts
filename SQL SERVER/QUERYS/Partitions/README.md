# What Is Partitioning?

#### the data of patitioned tables and indexes in divided into units that can be spread across more that one filegroup in a database . the data is partitioned so that "groups of rows" are mapped into individual partitions.

#### table patitioning is An Enterprise Edition Feature only

# Steps to create SQL Server Partitioned Table

## Create file Groups in individual disk drive(if possible)

## create a partition function

## create a partition schema

## partition the tabvle based on datetime column

# Benifits of partitioning

## improves the performance of sql queries , data managment , index managment and storage managment

## alow you to spread data onto diffrent physical disks,leveraging the concurrent performance of those disk to optimize query performance.

## seprate the data into partition based on data and then only index the most current date data

## translate or access subsets of data quickly and efficently

## You can perform maintenance operations on one or more partitions more quickly

## operations are more efficent becuase they target only these data subsets,instead of the whole table .

## when SQL server accesses one drive at a time , and this might reduce performance.

## improve performance by enabling lock escalation at the partition level to reduce lock contention on the table .

## extraction , moving , removing information from table with hndreds of milions of rows without creating blockages.

## partitioning is not visible to end users, a partitioned table behaves like one logical table when queried.

## less frequently accessed data can be placed on slower disks and more frequently accessed data can be placed on faster disks.

## historical , unchanging data can be set to read-only and then be excluded from regular backups .

## if data needs to be restored it is possible to restore the partitions with the most critical data first.
