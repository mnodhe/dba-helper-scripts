-- What range should we create partitions
-- we must find data count by each years in database
SELECT DISTINCT YEAR(TransactionDate) AS year,
    COUNT(*) AS Total_Rows
FROM [AdventureWorks2016].production.TransactionHistoryArchive
GROUP BY YEAR(TransactionDate)
ORDER BY 1;
/* RESULT
 YEAR  datacount
 2011	14688
 2012	40958
 2013	33607 
 */
-- so it's better to have 4 hard drive for each partition (in production) 
-- but for Demo , we create seperate newfolders named "drive d","drive e","drive f","drive g" in your drive
-- we create 4 file groups for each year partition
USE master
go ALTER DATABASE AdventureWorks2016
ADD FILEGROUP [DRIVE D]
go ALTER DATABASE AdventureWorks2016
ADD FILEGROUP [DRIVE E]
go ALTER DATABASE AdventureWorks2016
ADD FILEGROUP [DRIVE F]
go ALTER DATABASE AdventureWorks2016
ADD FILEGROUP [DRIVE G]
go;
-- so we created the file groups (containers)
-- with in those four folder , create 4 seperate data files (.ndf) for each year in table
-- create a partition function for each year range (NOTE: notable has been assigned to any partition as of yet)
-- thus you can associate any table to the partition at this point.
-- 2 create 4 data (NDF) files in filegroup for each partition
USE master
go ALTER DATABASE AdventureWorks2016
ADD FILE (
        NAME = N 'TRANS2005',
        FILENAME = N 'C:\drive d\TRANS2005.ndf',
        SIZE = 4096KB,
        FILEGROWTH = 1024KB
    ) TO FILEGROUP [DRIVE D]
GO ALTER DATABASE AdventureWorks2016
ADD FILE (
        NAME = N 'TRANS2006',
        FILENAME = N 'C:\drive e\TRANS2006.ndf',
        SIZE = 4096KB,
        FILEGROWTH = 1024KB
    ) TO FILEGROUP [DRIVE E]
GO ALTER DATABASE AdventureWorks2016
ADD FILE (
        NAME = N 'TRANS2007',
        FILENAME = N 'C:\drive f\TRANS2007.ndf',
        SIZE = 4096KB,
        FILEGROWTH = 1024KB
    ) TO FILEGROUP [DRIVE F]
GO ALTER DATABASE AdventureWorks2016
ADD FILE (
        NAME = N 'TRANS20015',
        FILENAME = N 'C:\drive g\TRANS2015.ndf',
        SIZE = 4096KB,
        FILEGROWTH = 1024KB
    ) TO FILEGROUP [DRIVE g]
GO -- at this point we have created the containers and files for the table to be partitioned too!!
    -- create partition on table [transactionhistoryarchive]
    USE [AdventureWorks2016]
GO BEGIN TRANSACTION CREATE PARTITION FUNCTION [FN_HISTORY](datetime) AS RANGE LEFT FOR
VALUES (
        N'2011-12-31T23:59:59.997',
        N'2012-12-31T23:59:59.997',
        N'2013-12-31T23:59:59.997'
    ) CREATE PARTITION SCHEME [SCH_HISTORY] AS PARTITION [FN_HISTORY] TO (
        [[DRIVE D]]],
        [[DRIVE E]]],
        [[DRIVE F]]],
        [[DRIVE G]]]
    )
ALTER TABLE [Production].[TransactionHistoryArchive] DROP CONSTRAINT [PK_TransactionHistoryArchive_TransactionID] WITH (ONLINE = OFF)
ALTER TABLE [Production].[TransactionHistoryArchive]
ADD CONSTRAINT [PK_TransactionHistoryArchive_TransactionID] PRIMARY KEY NONCLUSTERED ([TransactionID] ASC) WITH (
        PAD_INDEX = OFF,
        STATISTICS_NORECOMPUTE = OFF,
        SORT_IN_TEMPDB = OFF,
        IGNORE_DUP_KEY = OFF,
        ONLINE = OFF,
        ALLOW_ROW_LOCKS = ON,
        ALLOW_PAGE_LOCKS = ON
    ) ON [PRIMARY] CREATE CLUSTERED INDEX [ClusteredIndex_on_SCH_HISTORY_637501207747071375] ON [Production].[TransactionHistoryArchive] ([TransactionDate]) WITH (
        SORT_IN_TEMPDB = OFF,
        DROP_EXISTING = OFF,
        ONLINE = OFF
    ) ON [SCH_HISTORY]([TransactionDate]) DROP INDEX [ClusteredIndex_on_SCH_HISTORY_637501207747071375] ON [Production].[TransactionHistoryArchive] COMMIT TRANSACTION