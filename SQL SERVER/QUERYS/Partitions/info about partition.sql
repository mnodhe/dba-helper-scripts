SELECT ps.name
FROM sys.partition_schemes ps
    INNER JOIN sys.partition_functions pf ON pf.function_id = ps.function_id
    INNER JOIN sys.partition_range_values prf ON pf.function_id = prf.function_id;
------------------------------------------------------------------------------------
-- even better One
SELECT o.name objectname,
    i.name indexname,
    p.partition_id,
    p.partition_number,
    [rows]
FROM sys.partitions p
    INNER JOIN sys.objects o ON o.object_id = p.object_id
    INNER JOIN sys.indexes i ON i.object_id = p.object_id
    AND p.index_id = i.index_id
WHERE o.name LIKE '%TransactionHistoryArchive%'