Create table Products (
    ProductsId int IDENTITY (1, 1) Primary KEY,
    ProductName varchar (100),
    Brand varchar (100)
)
go