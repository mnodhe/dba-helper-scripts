Use master
Go CREATE DATABASE [NewDatabase] CONTAINMENT = NONE ON PRIMARY (
        NAME = N 'NewDatabase',
        FILENAME = N'C:\Path\to\file.mdf',
        SIZE = 5000MB,
        FILEGROWTH = 100MB
    ) LOG ON (
        NAME = N 'NewDatabase_log',
        FILENAME = N'C:\Path\to\file_log.ldf',
        SIZE = 100MB,
        FILEGROWTH = 10MB
    )