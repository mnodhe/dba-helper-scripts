# WHEN TO DO !!!

## when table is large in size (row count)

### contains milions if not 100's of milions rows

## when the tabale is used frequently

### table is used more than just infrequently

## whent the columns are frequently used in the WHERE clause

### the query againts the database has where clause

## Columns that are frequently referenced in ORDER BY and GROUP BY clause

### if you are sorting data then is's beneficial

## indexes should be created on columns with a high number of unique values

### create indexes on columns that are all virtually unique such as integer

## create a single column index on single where clause Query

### if the were clause has a single column then create a single index

## create a multiple column index om multiple where clause query

### if the where clause has multiple columns then create multiple index

## build index on columns of integer type

### integer take less space to store ,which means the query will be faster.

#### testing is The key in determining what will work for your environment . play with diffrent combinations of index , no indexes , single-column index and composite indexes.

# WHEN NOT TO DO!!!

## indexes should not be used on small tables

### smaller table do better with a "table scan" (regular select without index)

## indexes should not be used on columns that contain high number of NULL values.

### maintenance on the index can become excessive

## dont use an index that will return a high percentage of data

### few distinct values such as gender

## creating indexes come with a cost

### indexes take up disk space.

### insert,update and delete operations become slower, since the database system need to update the values in the table , and it also needs to upgrade the indexes
