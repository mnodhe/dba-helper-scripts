--Example of creating clustered index
--create phonbook tbl in SQL2 Database (refer to create fake Data folder)
create table [dbo].[PhoneBook](
    [phoneBookId] int null,
    lname varchar(50) null,
    Fname varchar(50) null,
    Phone varchar(50) null
) on [primary]
go ;
--insert 1000 records Using RedGate

-- lname must not have number in it

--Example of creating clustered index
create clustered index [Idx_PhoneBook_Lname] --Name the Index Like Idx_tableName_Columnname
on [dbo].[PhoneBook] (Lname ASC,Fname ASC);-- you can  use multiple columns

-- select the table and see all data sorted by Lname column ASC
select * from PhoneBook;
-- so if we insert some new data
   insert into [dbo].[PhoneBook] values(1001,'bagher','naghie','091207878314124');
   insert into [dbo].[PhoneBook] values(1002,'cine','naghie','09120787812312');
   insert into [dbo].[PhoneBook] values(1003,'dina','naghie','091207878512341');
   insert into [dbo].[PhoneBook] values(1004,'bina','naghie','09120787834763');

-- and select the table 
select * from PhoneBook;
-- you can see that data is automatically sort that data
-- so you can have ONE clustered index in a table.  NOT MORE !!!