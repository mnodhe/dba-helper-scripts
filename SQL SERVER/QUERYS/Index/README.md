# TYPE of Index (index are used in Tables Or Views)

## Clustered index

### clusterd indexes sort the data row in the table or view based on their key values as such there can be only one clustered index per table . when a table does not have an index it is referred to as a heap.when you create a clustered index it does not require any additional disk space

## non-Clustered index

### a nonclustered index have a structure seperate from data rows,much like the back index of a book , and as such does require disk space, as it's a separate object. a nonclustered index contains key value and each key value entry has a pointer to the data row that contains the key values.the pointer from an index row in a nonclustered index to a data row is called a row locator . the structure of the row locator depends on whether the data pages are stored in a heap or a clustered table , for heap a row locator is a pointer to the row . for clustered table , the row locator is the clustered index key.

## Composite Clustered index

### a composite index is an index in a index on two or mor columns of a table . you should consider performance where creating a composite index becuase the order of columns in the index has a measurable effect on data retrival speed. generally the most restrictive value should be placed first for optimum performance . however the columns that will always be specified should be placed first.

# Other less used Indexes (depends On Company)

## Covering index

## Full-text index

## Filtered index

## Column-Based index

## Spatial index

## XML index
