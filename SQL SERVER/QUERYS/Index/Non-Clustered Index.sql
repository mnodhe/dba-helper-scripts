--Example of creating nonclustered index
-- you can create multiple nonclustered index
create nonclustered index [NC_Idx_PhoneBook_Lname] --Name the Index Like Idx_tableName_Columnname
on [dbo].[PhoneBook] (fname ASC);

create nonclustered index [NC_Idx_PhoneBook_Lname] --Name the Index Like Idx_tableName_Columnname
on [dbo].[PhoneBook] (Phone ASC);

-- unlike the clustered index , you cannot see any change in table by select 
select * from PhoneBook;
-- because non clustered index not orgranize table . it create new object table that orgnaized.