use [ShrinkDB]
go create table initial (
        col1 int identity,
        col2 char (8000) DEFAULT 'front'
    );
go
insert into initial DEFAULT
values;
go 1500
select *
from initial;
sp_helpdb [ShrinkDB];
create TABLE second(
    col1 int identity,
    col2 char (8000) DEFAULT 'after'
);
go create clustered index col1 on second(col1);
go
insert into second DEFAULT
values;
go 1500;
select *
from second;
sp_helpdb [ShrinkDB];
select []
from sys_dm_db_index_physical_state;
(
    DB_ID(N'ShrinkDB'),
    OBJECT_ID(N'second'),
    1,
    null,
    'limited'
);
go;