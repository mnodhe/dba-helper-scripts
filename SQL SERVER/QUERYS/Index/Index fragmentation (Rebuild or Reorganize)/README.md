# Index Fragmentation (Rebuild or reorganize an Index)
# NOTE : Allways Do index Rebuild or reorganize OFF Hour !! it's resourse intense!!!

## when index fragmentations occurs should you reorgnize or rebuild a fragmented index? (It Depends on avg_fragmentation_in_percenty)

## whenever an insert - update or delete operations occur against the underlying data, sql automatically maintains indexes

## these operation cause index fragmentation and can cause preformance issues

## fragmentation exist when indexes have pages in which the logical ordering, based on the key value , does not match the physical ordering inside the data file

## when you rebuild an index sql drops and re-create the index and removes fragmentation, reclaims disk space by compactiong and reorders the index ROWS in contiguos pages

## when you ReOrganize an index it defragments the leaf level of clustered and nonclustered indexes on tables and views by physically reordering the leaf-level

## the system function sys_dm_db_index_physical_state alow you to detect fragmentation.

### if avg_fragmentation_in_percenty value

#### >5% and <=30% REORGANIZE

#### > 30% REBUILD

## rebuilding an index can be executed online or offline

## reorganizing an index is always excuted online.
