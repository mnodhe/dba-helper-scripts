-- let's select a data from Lname in peopletbl with 50M Record TABLE (refer to create fake data)

select * from [dbo].[People] where FName='ali1007';--replace fname ali1007 with a data from your table
-- it take about 41 seconds to select that data in 50M data record
-- SOOOOOOO lets Create Composite Index on our Peopletbl with 50M Record TABLE!! 
create nonclustered index [NC_INX_People_Lname_Fname] -- the more column you chose to index the longer it will take to create the index!!!!!!
on [dbo].[People] (Lname asc, Fname asc) -- note the order of the column specified is important. place the column in order of the query's where clause order
-- it should take about 4 min


--NOTE : create an index on Lname(to create  an index on Lname column the data has to be sorted and has pointers to the actual data in a table TABLE)
--the creation of an index can take along time becuase it is "COPYING" the column lname seperately with pointer to data!!!
-- any time you update the table people with andinsert update or delete the index on that column has to be updated ALSO.


