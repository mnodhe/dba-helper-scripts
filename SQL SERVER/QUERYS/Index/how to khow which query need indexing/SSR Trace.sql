-- show all trace properties
select * from sys.traces;


-- 0= stop the trace 
exec sp_trace_setstatus 2,0

-- 1= start the trace 
exec sp_trace_setstatus 2,1

-- 2 = delete the trace 
exec sp_trace_setstatus 2,2