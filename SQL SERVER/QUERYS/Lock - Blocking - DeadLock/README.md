# what is Lock - Blocking AND DeadLock ?

## Lock

### applicatios use lock to control data integrity in multi-user cocurrency

### locks perevent data from being modified by TWO SIMULTANUES SESSION

### a lock is a placed on an object (row,page, extent,table,database) by the sql server where any connection access the same piese of data cocurrently

## Blocking

### blocking occurs when one session has a lock on an object and thus causes another session to wait in a holding queue untill the current session is entirely done with the resources.

## DeadLock

### deadlocks occur when a session "A" is executing a process and where waiting for resources used by the other transaction and at the same time another session "B" is executing a process and is waiting for resource used by the other .

# what causes locks - blocking and deadlock.

## Poor database Design can cause crippling database lock connection.

## Poor Indexing Strategy.

## Query Implementitions problem.

## Table are not completely normalized.

## Optimize Transact-SQl code.

# The most Common Lock modes.

## Exclusive Locks (X)

### is placed on database object whenever it is updated with an insert or update statement

## Shared Locks (S)

### Is put to a database object wheenever it is being read(using the select statement)

## Update Locks (U)

### which can be thought of as an in-between mode between shared and exclusive lock

#### the main purpose of the update lock is to prevent deadlocks where multiple users simultaneosly try to update data

## Intent Locks (I)

### are used to indicate that a certian lock will be later placed on a certain database object . intent lock are used because locks can form hierarchies.

## Schema Locks (two types, SCH-M amd SCH-S)

### are used to prevent changes to object structure.

## Bulk Update Locks(BU)

### bulk update lock (BU) are used wen updating or inserting multiple row using a bulk update .

# Because maintaining locks can be an expensive operation performance-wise, SQL Server support a feature called multigranular locking . this means that locks can be placed on different levels , depending on the situation.
