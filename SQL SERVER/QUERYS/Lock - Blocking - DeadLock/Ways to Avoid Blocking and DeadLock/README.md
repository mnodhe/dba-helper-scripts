# Ways to Avoid Blocking and DeadLock

## use clustered indexes on high-usage tables.

## Break long transactions up into many shorter transactions.

## make sure that UPDATE and DELETE statement use an existing index.

## if your app's code allows a running query to be cancelled , it is important that the code alse roll back the transaction.

