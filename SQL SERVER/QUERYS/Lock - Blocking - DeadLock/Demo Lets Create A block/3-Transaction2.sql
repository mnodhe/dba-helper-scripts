begin tran
update TableOne
set fname = "mary"
where id = 1;
commit transaction 
-- notice that the execution take long time and do not complete.
-- thats how you make a exclusive lock
-- SO Let's #see what make the blocking #
SP_WHO2 -- so  now go to step 2 and run the commit and  watch what happen to the transaction2
-- as you see the transaction2 instantly completed after the lock from transaction1 released
-- or you can Kill the SPID you get from SP_WHO2 but notice that all transaction for that query will be roled back .
-- Contact the user before Killing the process.