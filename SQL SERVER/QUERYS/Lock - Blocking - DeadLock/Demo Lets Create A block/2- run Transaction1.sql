begin tran
update TableOne
set fname = "mary"
where id = 1;
-- We MUST Run on of two line below but we did end the transaction so the row id 1 is locked.
-- go to step 3
--rollback 
--commit transaction