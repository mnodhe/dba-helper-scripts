DBCC SHOW_STATISTICS ('table', 'INDEX') WITH HISTOGRAM;
-- list all of the statistics being maintained on a table
exec sp_helpstats
@objectname = 'tablename',
@result = 'ALL';