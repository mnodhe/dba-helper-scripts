-- ALL DBCC CHECKDB 
DBCC CHECKDB (
    'dbname' [,NOINDEX
    |{
        REPAIR_ALLOW_DATA_LOSS|
        REAPAIR_FAST|
        REPAIR_REBUILD
    }
    ]
) [WITH {
[ALL_ERRORMSGS] [,[NO_INFOMSGS]]
[,[TABLOCK]]
[,[ESTIMATEONLY]]
[,[PHYSICAL_ONLY]]
}
] 
-- watch your resource . it's resource intense. bu it should be done to get base line metric.
DBCC CHECKDB ('dbname') -- Good Result: CHECKDB found 0 allocation errors and 0 consistency errors in database .
----------------------------------------------------------------------------------------------------------------
--specifies that non clustered indexes for non system table should not be checked.
DBCC CHECKDB ('dbname', NOINDEX) ----------------------------------------------------------------------------------------------------------------