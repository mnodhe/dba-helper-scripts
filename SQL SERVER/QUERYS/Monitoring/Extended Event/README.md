# Extended Event was introduced in 2008 but with no GUI . in 2012 the introduction of the GUI made the life easier.

## Better than SQL Profiler. CUZ . it has little performance impact

# Benefits of using the Extended Events

## Extended Events Built into SSMS

## Extended Events sessions canbe created without any T-SQL commands or query XML data.

## HARDLY ANY OVERHEAD WHEN USING THEM ON THE SQL SERVER

## Less than 2% of CPU's resource.

## >> Replace SQL Profiler and SQL Trace. <<

## Easy to use and powerful (Wizard driven)

# The Reason to use Extended Events

## finding long-Running Queries

## Tracking DDL Operations

## Find Missing Statistics

## Resolving and finding blocking and deadlocking

## Queries that cause specific wait states to occur

## Monitoring SQL SERVER memory stress
