use sql
go
;with Empty AS (
    select OBJECT_NAME(OBJECT_ID) [Table],
        sum(row_count) [Records]
    from sys.dm_db_partition_stats
    where index_id = 0
        or index_id = 1
    group by OBJECT_ID
)
select [Table],
    Records
from [Empty]
where [Records] = 0