-- index usage 
-- used index
select object_name(ddius.[object_id], ddius.database_id) as [object_name],
    ddius.index_id,
    ddius.user_seeks,
    ddius.user_scans,
    ddius.user_lookups,
    ddius.user_seeks + ddius.user_scans + ddius.user_lookups as user_reads,
    ddius.user_updates as User_writes,
    ddius.last_user_scan,
    ddius.last_user_lookup
from SYS.DM_DB_INDEX_USAGE_STATS ddius
where ddius.database_id > 4 -- filter out system tables
    and objectproperty(ddius.object_id, 'IsUserTable') = 1
    and ddius.index_id > 0 -- filter out heaps
order by ddius.user_scans desc

 -- unused indexes
select object_name(i.[object_id]) as [Table Name],
    i.name
from sys.indexes as i
    inner join sys.objects as o on i.[object_id] = o.[object_id]
where i.index_id not in (
        select ddius.index_id
        from sys.dm_db_index_usage_stats as ddius
        where ddius.[object_id] = i.[object_id]
            and i.index_id = ddius.index_id
            and database_id = DB_ID()
    )
    and o.[type] = 'U'
order by OBJECT_NAME(i.[object_id]) asc;