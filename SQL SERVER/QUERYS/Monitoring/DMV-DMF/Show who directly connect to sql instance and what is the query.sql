select dec.client_net_address,
    des.host_name,
    des.context_info
from sys.dm_exec_sessions des
    inner join sys.dm_exec_connections dec on des.session_id = dec.session_id
    cross apply sys.dm_exec_sql_text(dec.most_recent_sql_handle) dest
where des.program_name like 'Microsoft SQL Server Management Studio%'
order by des.program_name,
    dec.client_net_address