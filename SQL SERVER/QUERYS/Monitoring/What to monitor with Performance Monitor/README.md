# In CPU

## Processor > Processor Time

### value should be consistently less than 90%. a consistent value of 90% or more indicates a cpu bottleneck.

## system > Processor Queue Length

### the number of threads waiting for processor time . a consistent value of 2 or more can indicates a processor bottleneck

#### in most cases a processor bottleneck indicates you need to either upgrade to a faster processor or to multiple processors. in some cases , you can reduce the processor load BY FINE TUNING THE APPLICATION!!!

# In MEMORY (SQL in Memory Hungry)

## MEMORY > Available MBytes

### the higher the free value the better

## MEMORY > Page/sec

### indicate the number of pages that were retrived from disk . high value can indicate lack of memory

## MEMORY > Page Faults/sec

### make sure that the disk activity is not coused by paging

## SQL Server > Buffer Manager> Buffer Cache Hit Ratio

### a Rate of 90% of higher is desirable. a value greater than 90% indicates that more than 90% of all requests for data were satisfied from data cache rather than the disk

## SQL Server > Memory Manager> Total Server Memory (KB)

## SQL Server > Buffer Manager> Page Life Expectancy

### the number of seconds a page will stay in the buffer pool without references. the longer the page stays om the buffer pool the better peformance you will get rather getting the data from the disk. the recommended value of the page life expectancy counter is approximately 300 seconds.

# Disk

## Avg, Disk sec/Read

## Avg, Disk sec/Write

### the Average Should be below 15ms to max 30ms. the less timeit takes the faster your system will be.
