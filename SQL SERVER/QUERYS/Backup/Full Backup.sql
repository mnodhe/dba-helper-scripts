BACKUP DATABASE [DBNAME] TO DISK = N'C:\PATH\TO\DB.bak' WITH NOINIT,
    NAME = N'BackupDatabase=full Database Backup',
    COMPRESSION,
    STATS = 10
GO