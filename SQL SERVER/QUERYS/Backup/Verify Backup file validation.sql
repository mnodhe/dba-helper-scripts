Use master
Go
declare @backupSetId as int
select @backupSetId = position
from msdb..backupset
where database_name = N'yourdbname'
    and backup_set_id =(
        select max(backup_set_id)
        from msdb..backupset
        where database_name = N'yourdbname'
    ) if @backupSetId is null begin raiserror(
        N'Verify Failed.Backup information for database ' 'BackupDatabase'' not found',
        16,
        1
    )
end RESTORE VERIFYONLY
FROM DISK = N'C:\PATH\TO\BACKUUP.BAK' WITH FILE = @backupSetId
GO 
-------------------------------------- BOUNUS ------------------------------------------
--------------------------- See All BackUped File Info ---------------------------------
Use msdb
go
SELECT logical_name,
    physical_name,
    file_number,
    backup_size,
    file_type,
    *
from dbo.backupfile
order by 1