-- this can only be taken if the database recovery model is in full mode 
-- and a full database backup had been executed
BACKUP LOG [DBNAME] TO DISK = N'C:\PATH\TO\DB.bak' WITH NAME = N'BackupDatabase=Transaction Log Backup',
STATS = 10
GO