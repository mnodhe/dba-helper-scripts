-- 1 - see if mail is active on server 
--SHOW option for server properties (about 17)
sp_CONFIGURE
GO
-- 2 SHOW option for server advanced properties (about 69)
sp_CONFIGURE 'Show Advanced',1
GO
RECONFIGURE
GO
-- 3 see all config
sp_CONFIGURE
GO
-- 4 active database mail
sp_CONFIGURE 'Database Mail XPs',1
GO
RECONFIGURE
GO
-- 5 see all config for database mail xps value
sp_CONFIGURE
GO
-- 6 close Showing option for server advanced properties to server default properties
-- !!! For Security Reasons!!!
sp_CONFIGURE 'Show Advanced',0
GO
RECONFIGURE
GO
-- 7 Check if it is done correctlly
sp_CONFIGURE
GO
-- 8 now you go to ssms > management > database mail>configure
-- 9 create a operator for email
-- 10 add operator to a job notification