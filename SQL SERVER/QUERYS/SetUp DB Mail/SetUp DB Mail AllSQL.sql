-- 1 enable database mail

execute sp_configure 'show advanced',1;
RECONFIGURE;
execute sp_configure 'Database Mail XPs',1;
RECONFIGURE;
go;
execute sp_configure 'show advanced',0;
RECONFIGURE;
go;
-- 2 create a database mail account
execute msdb.dbo.sysmail_add_account_sp
@account_name ='SQL',
@description = 'Account used by all mail profiles.',
@email_address='mnodhe@gmail.com',--enter your email address here
@display_name='Database Mail',
@mailserver_name='smtp.gmail.com';--enter your server name here
-- 3 create a db mail profile
execute msdb.dbo.sysmail_add_profile_sp
@profile_name='Default Public Profile',
@description='Default Public Profile for all users';
-- 4 add the account to the profile 
execute msdb.dbo.sysmail_add_profileaccount_sp
@profile_name='Default Public Profile',
@account_name='SQL',
@sequence_number=1;
-- 5 grant access to the profile to all msdb database users
execute msdb.dbo.sysmail_add_principalprofile_sp
@profile_name='Default Public Profile',
@principal_name='public',
@is_default=1;
-- 6 send test email
execute msdb.dbo.sp_send_dbmail
@subject='mnodhe@gmail.com', -- where you want to send email
@recipients='mnodhe@gmail.com' -- enter your email address here
@query='select @@VERSION' ;--gives you the version of SQL in email
go